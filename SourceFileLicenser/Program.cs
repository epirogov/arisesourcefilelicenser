//autor: Eugene Pirogov
//email: eugene.intalk@gmail.com
//license: EULA
//date:04/12/2014
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace SourceFileLicenser
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// sfl using
			if(args.Length == 0)
			{
				Console.WriteLine ("Using for SFL utility source file licenser.");
				Console.WriteLine ("> sfl [file1] [file1]..");
				Console.WriteLine ("Write licese info for selected files.");
				Console.WriteLine ("> sfl [dir1] [dir2]..");
				Console.WriteLine ("Write licese recursiveliy to directory files.");
				Console.WriteLine ("> sfl ext:cs [dir1] [file1]");
				Console.WriteLine ("Write licese only for files with cs extension.");
			}

			var files = new List<string>();
			var dirs = new List<string>();
			var extensions = new List<string>();
			var warnings = new List<string>();
			var arguments = args.ToList();

			// process arguments
			arguments.ForEach(arg => {
				if(File.Exists(arg))
				{
					files.Add(arg);
				}
			});

			arguments.ForEach(arg => {
				if(Directory.Exists(arg))
				{
					dirs.Add(arg);
				}
			});

			arguments.ForEach(arg => {
				if(arg.ToLower().StartsWith("ext:"))
				{
					extensions.Add(arg.Substring("ext:".Length));
				}
			});

			arguments.ForEach(arg => {
				if(!files.Contains(arg) && 
					!dirs.Contains(arg) &&
					!extensions.Contains(arg.Replace("ext:", string.Empty)))
				{
					warnings.Add(arg);
				}
			});

			// process errors
			if(warnings.Count > 0)
			{
				Console.WriteLine ("Stopped : some arguments was not recognized as file, directory or extension.");
				Console.WriteLine ("Error list :");
				warnings.ForEach(arg => Console.WriteLine (arg));
				return;
			}

			//processing
			var licenseText = string.Format("//author:{0}{1}//email:{2}{3}//license:{4}{5}//date:{6}{7}",
				ConfigurationManager.AppSettings["author"],
				Environment.NewLine,
				ConfigurationManager.AppSettings["email"],
				Environment.NewLine,
				ConfigurationManager.AppSettings["license"],
				Environment.NewLine,
				DateTime.Now.ToShortDateString(),
				Environment.NewLine
			);

			var extOnly = extensions.Count > 0;
			dirs.ForEach(dir => files.AddRange(GetDirFilesRecursively(dir)));
			var list = !extOnly? files.Distinct() : files.Where(file => extensions.Contains( new FileInfo(file).Extension.Length > 0 ? new FileInfo(file).Extension.Substring(1) : string.Empty )).Distinct();

			if(list.Count() == 0)
			{
				Console.WriteLine ("sfl exit:no files found");
				return;
			}
			Console.WriteLine ("Processing:");
			foreach(var file in list.Distinct())
			{
				var text = File.ReadAllText(file);
				if(!HasLicense(text))
				{
					text = licenseText + text;
					File.WriteAllText(file, text);
					Console.WriteLine (file);
				}
			}
		}


		static bool HasLicense(string text)
		{
			return text.Length > "author".Length ? text.Substring(0,"author".Length).ToLower() == "author" : false;
		}


		static List<string> GetDirFilesRecursively(string dir)
		{
			var files = new List<string>();
			var dirs = new Stack<DirectoryInfo>();
			dirs.Push(new DirectoryInfo(dir));
			while(dirs.Count > 0)
			{
				var d = dirs.Pop();
				files.AddRange(d.GetFiles().Select(f => f.FullName));
				d.GetDirectories().ToList().ForEach(c => dirs.Push(c));
			}
			return files;
		}
	}
}
